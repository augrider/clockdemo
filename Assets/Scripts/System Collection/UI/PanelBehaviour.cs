using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UIElementsSystem
{
    public enum PanelState { On, Moving, Off, Expand };


    [RequireComponent(typeof(Panel))]
    public abstract class PanelBehaviour : MessageBehavior
    {
        public Panel panel { get; private set; }
        protected int panelID => gameObject.GetInstanceID();

        public bool isMoving => panel.isMoving;
        public PanelState panelState { get; private set; }


        protected override void Awake()
        {
            base.Awake();
            panel = GetComponent<Panel>();

            panelState = PanelState.Off;
        }


        public void SetOn()
        {
            panelState = PanelState.On;
            OnSetOn();
        }

        public void SetOff()
        {
            panelState = PanelState.Off;
            OnSetOff();
        }

        public void SetMoving()
        {
            panelState = PanelState.Moving;
            OnSetMoving();
        }

        public void SetExpand()
        {
            panelState = PanelState.Expand;
            OnSetExpand();
        }


        public void MoveTo(Vector2 target, PanelState finalState)
        {
            panelState = PanelState.Moving;

            panel.MoveTo(target);

            panelState = finalState;
        }

        public void MoveToRelative(Vector2 relativeTarget, PanelState finalState)
        {
            panelState = PanelState.Moving;

            panel.MoveToRelative(relativeTarget);

            panelState = finalState;
        }


        public void MoveTo(Vector2 target, Vector2 start, PanelState finalState)
        {
            panelState = PanelState.Moving;

            panel.MoveTo(target, start);

            panelState = finalState;
        }

        public void MoveToRelative(Vector2 relativeTarget, Vector2 relativeStart, PanelState finalState)
        {
            panelState = PanelState.Moving;

            panel.MoveToRelative(relativeTarget, relativeStart);

            panelState = finalState;
        }



        protected Action GetStateFunction(PanelState state)
        {
            switch (state)
            {
                default:
                case PanelState.On:
                    return SetOn;

                case PanelState.Off:
                    return SetOff;

                case PanelState.Expand:
                    return SetExpand;

                case PanelState.Moving:
                    return SetMoving;
            }
        }



        protected virtual void OnSetOn() { }
        protected virtual void OnSetOff() { }
        protected virtual void OnSetExpand() { }
        protected virtual void OnSetMoving() { }
    }
}