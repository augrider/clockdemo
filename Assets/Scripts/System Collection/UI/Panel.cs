using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TweenSystem;
using TweenSystem.General;
using System;

namespace UIElementsSystem
{
    public sealed class Panel : UIBehaviour
    {
        public RectTransform rectTransform { get; private set; }

        public bool isMoving => tweener ? tweener.isPlaying : false;

        public TweenerComponent tweener { get; private set; }
        private EffectMove effectMove;

        public Vector2 relativePosition => GetRelativePosition(rectTransform.anchoredPosition);
        public Vector2 absolutePosition => rectTransform.anchoredPosition;


        protected override void Awake()
        {
            this.rectTransform = GetComponent<RectTransform>();
            this.tweener = GetComponent<TweenerComponent>();

            if (tweener)
                CreateEffect();
        }


        public void SetPosition(Vector2 position)
        {
            rectTransform.anchoredPosition = position;
        }

        public void AddPosition(Vector2 position)
        {
            rectTransform.anchoredPosition += position;
        }


        public void SetRelativePosition(Vector2 relativePosition)
        {
            rectTransform.anchoredPosition = GetAbsolutePosition(relativePosition);
        }

        public void AddRelativePosition(Vector2 relativePosition)
        {
            rectTransform.anchoredPosition += GetAbsolutePosition(relativePosition);
        }



        public void MoveTo(Vector2 position)
        {
            if (tweener)
            {
                effectMove.SetParameters(rectTransform.anchoredPosition, position);
                tweener.Repeat();
                return;
            }

            rectTransform.anchoredPosition = position;
        }

        public void MoveTo(Vector2 position, Vector2 start)
        {
            if (tweener)
            {
                effectMove.SetParameters(start, position);
                tweener.Repeat();
                return;
            }

            rectTransform.anchoredPosition = position;
        }


        public void MoveToRelative(Vector2 relativePosition)
        {
            if (tweener)
            {
                effectMove.SetParameters(rectTransform.anchoredPosition, GetAbsolutePosition(relativePosition));
                tweener.Repeat();
                return;
            }

            SetRelativePosition(relativePosition);
        }

        public void MoveToRelative(Vector2 relativePosition, Vector2 relativeStart)
        {
            if (tweener)
            {
                effectMove.SetParameters(GetAbsolutePosition(relativeStart), GetAbsolutePosition(relativePosition));
                tweener.Repeat();
                return;
            }

            SetRelativePosition(relativePosition);
        }



        private void CreateEffect()
        {
            effectMove = new EffectMove(Vector3.zero, Vector3.zero);
            tweener.AddSyncedEffect(effectMove);
        }

        private Vector2 GetRelativePosition(Vector2 position)
        {
            return new Vector2(position.x / Screen.width, position.y / Screen.height);
        }

        private Vector2 GetAbsolutePosition(Vector2 relativePosition)
        {
            return new Vector2(relativePosition.x * Screen.width, relativePosition.y * Screen.height);
        }
    }
}