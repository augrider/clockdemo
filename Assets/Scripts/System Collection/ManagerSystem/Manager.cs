using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManagerSystem
{
    /// <summary>
    /// Base class for all managers and their loaders
    /// </summary>
    public abstract class Manager<T> : Singleton<T>, IManager where T : Manager<T>
    {
        public bool isLoaded { get; protected set; }

        public static bool IsLoaded => current.isLoaded;


        public static IEnumerator WaitForLoad()
        {
            while (!current.isLoaded)
                yield return null;
        }

        public static IEnumerator WaitForLoad(IManager manager)
        {
            if (manager == null)
                throw new NullReferenceException("Manager reference is empty!");

            while (!manager.isLoaded)
                yield return null;
        }


        public virtual void Load()
        {
            OnLoad();
            ToggleLoaded(true);
        }


        protected void ToggleLoaded(bool value)
        {
            isLoaded = value;
            if (value)
                enabled = true;
        }


        public abstract void OnLoad();
        public virtual float GetLoadPercentage() => 1;
    }
}