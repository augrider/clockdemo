using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ManagerSystem
{
    public interface IManager
    {
        bool isLoaded { get; }

        void Load();
        float GetLoadPercentage();
    }
}