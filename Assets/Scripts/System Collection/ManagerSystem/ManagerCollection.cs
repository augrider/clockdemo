using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace ManagerSystem
{
    /// <summary>
    /// Base class for all manager loaders
    /// </summary>
    public abstract class ManagerCollection<T> : Manager<T> where T : ManagerCollection<T>
    {
        private int loaded = 0;
        private int count = 1;

        [SerializeField] private int fullLoadCount = 1;
        [SerializeField] private bool fullLoadOnly = false;


        public override void Load()
        {
            OnLoad();
        }

        public override void OnLoad()
        {
            // if (isLoaded)
            //     throw new System.Exception(this + " already loaded!");

            StopAllCoroutines();
            StartCoroutine(LoadAllManagers());
        }

        public override float GetLoadPercentage() => loaded / count;



        private IEnumerator LoadAllManagers()
        {
            yield return null;

            var sequence = GetManagersSequence();

            Debug.LogWarningFormat("Manager sequence {0}", sequence.AllContent());

            count = sequence.Length;
            loaded = -1;

            while (loaded < count - 1)
            {
                loaded++;
                var current = sequence[loaded];

                if (current == null || current.isLoaded)
                {
                    Debug.LogWarningFormat("Module #{0}/{1} is null or already loaded!", loaded + 1, count);
                    continue;
                }

                current.Load();
                yield return WaitForLoad(current);

                Debug.LogWarningFormat("{0}: loaded {1}/{2}", this, loaded + 1, count);
                yield return null;
            }

            ToggleLoaded(loaded + 1 >= fullLoadCount || !fullLoadOnly);
        }



        protected abstract IManager[] GetManagersSequence();
    }
}