using System;
using System.Collections;
using UnityEngine;


/// <summary>
/// Contains scene data collection and handles it's load/unload
/// </summary>

[CreateAssetMenu(fileName = "NewScenePackage", menuName = "Scene Package", order = 151)]
public class ScenePackage : ScriptableObject
{
    [SerializeField] private SceneData[] sceneReferences;


    public IEnumerator Load(Action doBeforeLoad = null, Action doAfterLoad = null)
    {
        if (sceneReferences.Length <= 0)
            throw new ArgumentNullException("No scenes found in package!");

        doBeforeLoad?.Invoke();
        yield return null;

        foreach (SceneData sceneData in sceneReferences)
        {
            yield return sceneData.Load();
            yield return null;
        }

        yield return null;

        doAfterLoad?.Invoke();
        yield return null;
    }

    public IEnumerator Unload(Action doBeforeUnload = null, Action doAfterUnload = null)
    {
        if (sceneReferences.Length <= 0)
            throw new ArgumentNullException("No scenes found in package!");

        doBeforeUnload?.Invoke();
        yield return null;

        foreach (SceneData sceneData in sceneReferences)
            yield return sceneData.Unload();

        yield return null;

        doAfterUnload?.Invoke();
        yield return null;
    }



    private string GetName(SceneReference sceneRef)
    {
        string[] SceneSplittedName = sceneRef.ScenePath.Split('/');
        string SceneNameWithOutExtension = SceneSplittedName[SceneSplittedName.Length - 1].Split('.')[0];
        return SceneNameWithOutExtension;
    }
}