using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for providing global access point for single instance of this class
/// </summary>
/// <typeparam name="T">Class derived from Message Behavior</typeparam>
public abstract class Singleton<T> : MessageBehavior where T : MessageBehavior
{
    public static T current { get; private set; }


    protected override void Awake()
    {
        if (current)
            throw new TypeLoadException("More than one object of type " + this.GetType() + " detected!");

        base.Awake();
        current = this as T;
    }

    protected void OnDestroy()
    {
        current = null;
    }
}