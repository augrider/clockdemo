﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Reflection;

namespace MessageSystem
{
    public abstract class BaseMessage<T> : ICloneable<T> where T : BaseMessage<T>
    {
        private static List<SubscriberData<T>> subList;
        public int originalID => throw new NotImplementedException();


        public BaseMessage() { }


        public void Subscribe(MethodInfo method, MessageBehavior casterObj)
        {
            try
            {
                if (subList == null)
                    subList = new List<SubscriberData<T>>();

                Action<T> newDelegate = (Action<T>)Delegate.CreateDelegate(typeof(Action<T>), casterObj, method);
                subList.Add(new SubscriberData<T>(newDelegate, casterObj));
            }
            catch
            {
                throw new TypeAccessException(string.Format("Object {0}, method {1} is not Action<{2}>!", casterObj, method, typeof(T)));
            }
        }


        public void Call()
        {
            if (subList != null)
            {
                CheckForActivity();

                foreach (SubscriberData<T> data in subList.ToArray())
                    data.Invoke((T)this);
            }
            // else
            //     throw new NullReferenceException("No one subbed to " + this);
        }


        public T GetMessage() => this as T;
        public virtual T Copy() => this.MemberwiseClone() as T;



        private static void CheckForActivity()
        {
            for (int i = 0; i < subList.Count; i++)
                if (!subList[i].IsValid())
                {
                    subList.RemoveAt(i);
                    i--;
                }
        }
    }


    public struct SubscriberData<T> where T : BaseMessage<T>
    {
        public Action<T> subMethod;
        private MessageBehavior subClass;


        public SubscriberData(Action<T> subMethod, MessageBehavior subClass)
        {
            this.subMethod = subMethod ?? throw new ArgumentNullException(nameof(subMethod));
            this.subClass = subClass ?? throw new ArgumentNullException(nameof(subClass));
        }

        public bool IsValid()
        {
            return subClass ? subClass.isActiveAndEnabled : false;
        }

        public void Invoke(T callInfo)
        {
            subMethod?.Invoke(callInfo);
        }
    }
}