using System;

[AttributeUsage(AttributeTargets.Method)]
public class AutoSubscribeFunctionAttribute : Attribute { }