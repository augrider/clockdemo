﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;
using MessageSystem;


/// <summary>
/// MonoBehaviour с возможностью автоматической подписки на сообщения
/// </summary>
public class MessageBehavior : MonoBehaviour
{
    private static readonly BindingFlags m_bingingFlags = BindingFlags.Public | BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy;
    [HideInInspector] private List<SubMethodInfo> funcList = new List<SubMethodInfo>();

    [HideInInspector] private bool isSearched = false;


    protected virtual void Awake()
    {
        // AutoSubscribe();
    }

    protected virtual void OnEnable()
    {
        AutoSubscribe(this);
    }



    private static void AutoSubscribe(MessageBehavior behavior)
    {
        if (!behavior.isSearched)
        {
            FindAllFunctions(behavior);
            behavior.isSearched = true;
        }

        SubscribeAllMethods(behavior);
    }


    private static void FindAllFunctions(MessageBehavior behaviour)
    {
        var methods = behaviour.GetType().GetMethods(m_bingingFlags);

        foreach (MethodInfo methodInfo in methods)
        {
            var attribute = methodInfo.GetCustomAttribute<AutoSubscribeFunctionAttribute>();

            if (attribute != null)
            {
                foreach (ParameterInfo parameter in methodInfo.GetParameters())
                {
                    Debug.Log(behaviour.ToString() + " attempting to Subscribe()");
                    behaviour.funcList.Add(new SubMethodInfo(methodInfo, parameter.ParameterType));
                }
            }
        }
    }

    private static void SubscribeAllMethods(MessageBehavior behaviour)
    {
        foreach (SubMethodInfo subMethodInfo in behaviour.funcList)
            subMethodInfo.Subscribe(behaviour);
    }



    private struct SubMethodInfo
    {
        public MethodInfo funcToSubscribe;

        public Type messageType;
        public MethodInfo messageSubFunc;


        public SubMethodInfo(MethodInfo funcToSubscribe, Type messageType)
        {
            this.funcToSubscribe = funcToSubscribe;
            this.messageType = messageType;

            messageSubFunc = messageType.GetMethod("Subscribe");

            if (messageSubFunc == null)
                throw new ArgumentNullException("Subscribe method was not found!");
        }


        public void Subscribe(MessageBehavior casterObj)
        {
            var messageInstance = Activator.CreateInstance(messageType);

            object[] arguments = new object[] { funcToSubscribe, casterObj };
            messageSubFunc.Invoke(messageInstance, arguments);
        }
    }
}