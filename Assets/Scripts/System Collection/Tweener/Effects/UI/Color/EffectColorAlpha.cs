using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TweenSystem.UI
{
    public class EffectColorAlpha : EffectUI
    {
        private float startAlpha;
        private float targetAlpha;


        public EffectColorAlpha(float startAlpha = 0, float targetAlpha = 1)
        {
            this.startAlpha = startAlpha;
            this.targetAlpha = targetAlpha;
        }


        protected override void OnTweenDataSet(ITweenData tweenData)
        {
            GetGraphic();
        }


        protected override void OnStateChange(float lerpValue)
        {
            if (graphic)
                graphic.color = GetColorFrom(graphic.color, Mathf.Lerp(startAlpha, targetAlpha, lerpValue));
        }



        private Color GetColorFrom(Color original, float alpha = 1)
        {
            return new Color(original.r, original.g, original.b, alpha);
        }
    }
}