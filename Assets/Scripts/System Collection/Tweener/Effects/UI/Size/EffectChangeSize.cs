using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem.UI
{
    public class EffectChangeSize : EffectUI
    {
        private Vector2 start;
        private Vector2 target;


        public EffectChangeSize(Vector2 start, Vector2 target)
        {
            this.start = start;
            this.target = target;
        }

        public void SetParameters(Vector2 start, Vector2 target)
        {
            this.start = start;
            this.target = target;
        }


        protected override void OnTweenDataSet(ITweenData tweenData)
        {
            GetRectTransform();
        }


        protected override void OnStateChange(float lerpValue)
        {
            if (!rectTransform)
                return;

            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Mathf.Lerp(start.x, target.x, lerpValue));
            rectTransform.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, Mathf.Lerp(start.y, target.y, lerpValue));
        }
    }
}