using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace TweenSystem.UI
{
    public abstract class EffectUI : Effect
    {
        protected RectTransform rectTransform;
        protected Graphic graphic;


        protected RectTransform GetRectTransform()
        {
            rectTransform = gameObject.GetComponent<RectTransform>();
            return rectTransform;
        }

        protected Graphic GetGraphic()
        {
            graphic = gameObject.GetComponent<Graphic>();
            return graphic;
        }
    }
}