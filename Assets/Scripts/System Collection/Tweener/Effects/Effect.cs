using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem
{
    public abstract class Effect : BaseTweenData, IEffect
    {
        public override TweenStyle currentStyle => defaultStyle ? tweenData.currentStyle : _tweenStyle;
        public override float currentLength => defaultLength ? tweenData.currentLength : _length;

        private bool defaultStyle = true;
        private bool defaultLength = true;

        protected ITweenData tweenData { get; private set; }
        protected GameObject gameObject => tweenData?.gameObject;

        protected float currentLerp { get; private set; }


        public void SetTweenStyle(TweenStyle style)
        {
            this._tweenStyle = style;
            defaultStyle = false;
        }

        public void SetLength(float length)
        {
            this._length = length;
            defaultLength = false;
        }


        public void SetTweenData(ITweenData tweenData)
        {
            currentLerp = 0;

            this.tweenData = tweenData;

            OnTweenDataSet(tweenData);
        }


        public void ChangeState(float currentTime)
        {
            var lerp = GetCurrentLerp(currentTime);

            if (lerp == currentLerp)
                return;

            currentLerp = lerp;

            OnStateChange(currentLerp);
        }


        protected virtual void OnTweenDataSet(ITweenData tweenData) { }
        protected abstract void OnStateChange(float lerpValue);
    }
}