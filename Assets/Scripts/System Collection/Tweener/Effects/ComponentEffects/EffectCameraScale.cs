using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem.General
{
    public class EffectCameraScale : Effect
    {
        private float start;
        private float target;

        Action<float> setCameraScale;


        public EffectCameraScale(float start, float target, Action<float> setCameraScale)
        {
            this.start = start;
            this.target = target;

            this.setCameraScale = setCameraScale;
        }

        public void SetParameters(float start, float target)
        {
            this.start = start;
            this.target = target;
        }


        protected override void OnStateChange(float lerpValue)
        {
            setCameraScale?.Invoke(Mathf.Lerp(start, target, lerpValue));
        }
    }
}