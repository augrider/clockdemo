using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem.General
{
    public class EffectFlashSize : Effect
    {
        private Vector3 smallest = new Vector3(0.9f, 0.9f, 1);
        private Vector3 biggest = new Vector3(1.5f, 1.5f, 1);


        protected override void OnStateChange(float lerpValue)
        {
            if (lerpValue < 0.05f)
            {
                gameObject.transform.localScale = Vector3.Lerp(Vector3.one, smallest, MathCustom.IntervalRelPos(lerpValue, 0, 0.05f));
                return;
            }

            if (lerpValue < 0.5f)
            {
                gameObject.transform.localScale = Vector3.Lerp(smallest, biggest, MathCustom.IntervalRelPos(lerpValue, 0.05f, 0.5f));
                return;
            }

            gameObject.transform.localScale = Vector3.Lerp(biggest, Vector3.one, MathCustom.IntervalRelPos(lerpValue, 0.5f, 1));
        }
    }
}