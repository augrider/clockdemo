using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem.General
{
    public class EffectPopToScale : Effect
    {
        private Vector3 start = new Vector3(0, 0, 1);
        private Vector3 target = new Vector3(1, 1, 1);

        bool additionalPop = true;


        /// <summary>
        /// Default variant: pop from zero to one
        /// </summary>
        public EffectPopToScale(bool additionalPop = true)
        {
            this.additionalPop = additionalPop;
        }

        public EffectPopToScale(Vector3 start, Vector3 target, bool additionalPop = true)
        {
            this.start = start;
            this.target = target;

            this.additionalPop = additionalPop;
        }


        protected override void OnStateChange(float lerpValue)
        {
            var popScale = target + Vector3.one * 0.2f;

            if (lerpValue < 0.8f)
            {
                gameObject.transform.localScale = Vector3.Lerp(start, popScale, MathCustom.IntervalRelPos(lerpValue, 0, 0.8f));
                return;
            }

            gameObject.transform.localScale = Vector3.Lerp(popScale, target, MathCustom.IntervalRelPos(lerpValue, 0.8f, 1));
        }
    }
}