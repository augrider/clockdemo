using System.Collections;
using System.Collections.Generic;
using TweenSystem.UI;
using UnityEngine;

namespace TweenSystem.General
{
    public class EffectMove : EffectUI
    {
        private Vector3 start;
        private Vector3 target;


        public EffectMove(Vector3 start, Vector3 target)
        {
            this.start = start;
            this.target = target;
        }

        public void SetParameters(Vector3 start, Vector3 target)
        {
            this.start = start;
            this.target = target;
        }

        protected override void OnTweenDataSet(ITweenData tweenData)
        {
            GetRectTransform();
        }


        protected override void OnStateChange(float lerpValue)
        {
            if (rectTransform)
            {
                rectTransform.anchoredPosition = Vector3.Lerp(start, target, lerpValue);
                return;
            }

            gameObject.transform.localPosition = Vector3.Lerp(start, target, lerpValue);
        }
    }
}