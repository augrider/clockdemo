using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem.World
{
    public class EffectColorPulse : EffectWorld
    {
        private Color edge;
        private Color fill;

        bool getEdgeFromSprite = false;

        private float delayValue = 0.33f;
        private float transferValue => (1 - delayValue) / 2;


        public EffectColorPulse(Color edge, Color fill, float delayValue = 0.33f)
        {
            this.edge = edge;
            this.fill = fill;

            getEdgeFromSprite = false;
            this.delayValue = delayValue;
        }

        public EffectColorPulse(Color fill, float delayValue = 0.33f)
        {
            this.fill = fill;

            getEdgeFromSprite = true;
            this.delayValue = delayValue;
        }


        protected override void OnTweenDataSet(ITweenData tweenData)
        {
            GetSpriteRenderer();
            if (spriteRenderer && getEdgeFromSprite)
                edge = spriteRenderer.color;
        }


        protected override void OnStateChange(float lerpValue)
        {
            if (!spriteRenderer)
                return;

            if (lerpValue < transferValue)
            {
                spriteRenderer.color = Color.Lerp(edge, fill, MathCustom.IntervalRelPos(lerpValue, 0, transferValue));
                return;
            }

            if (lerpValue > transferValue + delayValue)
            {
                spriteRenderer.color = Color.Lerp(fill, edge, MathCustom.IntervalRelPos(lerpValue, transferValue + delayValue, 1));
                return;
            }
        }
    }
}