using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem.World
{
    public class EffectAlphaPulse : EffectWorld
    {
        private float delayValue = 0.33f;
        private float transferValue => (1 - delayValue) / 2;

        private bool isInverse = false;

        private Color startColor;


        public EffectAlphaPulse(float delayValue = 0.33f, bool isInverse = false)
        {
            this.delayValue = delayValue;
            this.isInverse = isInverse;
        }


        protected override void OnTweenDataSet(ITweenData tweenData)
        {
            GetSpriteRenderer();
        }


        protected override void OnStateChange(float lerpValue)
        {
            if (!spriteRenderer)
                return;

            startColor = GetColorFrom(spriteRenderer.color);

            if (lerpValue < transferValue)
            {
                spriteRenderer.color = Color.Lerp(ColorEdge(), ColorFill(), MathCustom.IntervalRelPos(lerpValue, 0, transferValue));
                return;
            }

            if (lerpValue > transferValue + delayValue)
            {
                spriteRenderer.color = Color.Lerp(ColorFill(), ColorEdge(), MathCustom.IntervalRelPos(lerpValue, transferValue + delayValue, 1));
                return;
            }
        }



        private Color ColorEdge()
        {
            if (isInverse)
                return GetColorFrom(startColor, 0);

            return startColor;
        }

        private Color ColorFill()
        {
            if (!isInverse)
                return GetColorFrom(startColor, 0);

            return startColor;
        }


        private Color GetColorFrom(Color original, float alpha = 1)
        {
            return new Color(original.r, original.g, original.b, alpha);
        }
    }
}