using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem.World
{
    public abstract class EffectWorld : Effect
    {
        protected SpriteRenderer spriteRenderer;


        protected SpriteRenderer GetSpriteRenderer()
        {
            spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
            return spriteRenderer;
        }
    }
}