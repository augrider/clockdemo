using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TweenSystem.World;
using TweenSystem.UI;


namespace TweenSystem
{
    /// <summary>
    /// Better way to manage simultaneous work of multiple tweeners
    /// </summary>
    public class TweenerGroupComponent : MonoBehaviour
    {
        private List<ITween> tweens = new List<ITween>();

        private Action doAfter;

        public bool isFixedLength => _isFixedLength;
        [SerializeField] private bool _isFixedLength = true;

        [SerializeField] private float length = 1;

        public PlayState playState => _playState;
        private PlayState _playState = PlayState.Stop;


        public void AddTween(ITween tween)
        {
            if (!tweens.Contains(tween))
                tweens.Add(tween);
        }


        public void SetLength(float value)
        {
            if (value > 0)
                length = value;
        }

        public void DoAfter(Action action = null) => doAfter = action;


        public void Play()
        {
            if (playState == PlayState.Play)
                return;

            _playState = PlayState.Play;
            StartCoroutine(PlayAndWait());
        }

        public void Stop(bool invokeAfter = false)
        {
            if (playState != PlayState.Play)
                return;

            _playState = PlayState.Stop;
            StopAllCoroutines();

            foreach (ITween tween in tweens)
                tween.Stop(invokeAfter, false);

            if (invokeAfter)
                doAfter?.Invoke();
        }



        private IEnumerator PlayAndWait()
        {
            foreach (ITween tween in tweens)
                tween.SetLength(length).Play();

            yield return new WaitForSeconds(length);

            if (_isFixedLength)
                Stop(true);
        }
    }
}