using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem
{
    public class SyncTween : Tween
    {
        public SyncTween(TweenerComponent tweener, TweenStyle tweenStyle, float delay, float length) : base(tweener, tweenStyle, delay, length) { }


        protected override IEnumerator PlayEffects()
        {
            if (validateFunc == null)
                yield return waitForDelay;
            else
                while (!validateFunc.Invoke())
                    yield return null;

            float currentTime = 0;

            while (true)
            {
                currentTime += Time.deltaTime;

                foreach (IEffect effect in effects)
                    effect.ChangeState(currentTime);

                if (CheckForEffectEnd(currentTime, currentStyle))
                {
                    foreach (IEffect effect in effects)
                        effect.ChangeState(currentLength);

                    Stop();
                }

                yield return null;
            }
        }
    }
}