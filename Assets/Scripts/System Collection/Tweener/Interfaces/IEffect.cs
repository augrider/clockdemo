using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem
{
    public interface IEffect
    {
        void SetTweenStyle(TweenStyle style);
        void SetLength(float length);

        void SetTweenData(ITweenData tweenData);

        void ChangeState(float currentTime);
    }
}