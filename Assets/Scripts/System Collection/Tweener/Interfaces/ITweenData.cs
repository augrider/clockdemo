using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem
{
    public interface ITweenData
    {
        GameObject gameObject { get; }

        TweenStyle currentStyle { get; }
        float currentLength { get; }
    }
}