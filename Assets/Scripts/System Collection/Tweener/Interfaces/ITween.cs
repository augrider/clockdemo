using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem
{
    public interface ITween
    {
        PlayState playState { get; }


        ITween AddEffect(IEffect effect);
        ITween AddEffect(IEffect effect, float length);
        ITween AddEffect(IEffect effect, TweenStyle tweenStyle);
        ITween AddEffect(IEffect effect, float length, TweenStyle tweenStyle);

        ITween RemoveEffect(IEffect effect);

        void ClearEffects();

        ITween SetTweenStyle(TweenStyle style);
        ITween SetLength(float length);
        ITween SetDelay(float delay);

        ITween WaitUntil(Func<bool> validateFunction);
        ITween DoAfter(Action action);
        ITween PlayAfter(ITween tween, float delay = 0f);

        void Play();
        void Pause();
        void Stop(bool invokeDoAfter = true, bool invokePlayAfter = true);
    }
}