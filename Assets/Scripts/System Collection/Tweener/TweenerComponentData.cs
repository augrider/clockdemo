using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem
{
    public enum TweenStyle { FixedTime, Loop, PingPong }

    public enum PlayState { Play, Pause, Stop }

    public abstract class TweenerComponentData : MonoBehaviour
    {
        [SerializeField] protected TweenStyle defaultTweenStyle = TweenStyle.FixedTime;
        [SerializeField] protected float defaultDelay = 0;
        [SerializeField] protected float defaultLength = 1;
    }
}