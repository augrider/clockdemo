using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace TweenSystem
{
    public abstract class BaseTweenData
    {
        protected TweenStyle _tweenStyle = TweenStyle.FixedTime;
        protected float _length = 1f;

        public abstract TweenStyle currentStyle { get; }
        public abstract float currentLength { get; }


        public float GetCurrentLerp(float currentTime)
        {
            var lerpUnclamped = currentTime / currentLength;

            switch (currentStyle)
            {
                default:
                case TweenStyle.FixedTime:
                    return FixedTimeLerp(lerpUnclamped);

                case TweenStyle.Loop:
                    return LoopLerp(lerpUnclamped);

                case TweenStyle.PingPong:
                    return PingPongLerp(lerpUnclamped);
            }
        }


        protected bool IsFixedEnd(float currentTime)
        {
            var lerpUnclamped = currentTime / currentLength;

            return lerpUnclamped >= 1;
        }



        private float PingPongLerp(float lerpUnclamped)
        {
            var intPart = Mathf.FloorToInt(lerpUnclamped);

            if (MathCustom.IsEvenNumber(intPart))
                return LoopLerp(lerpUnclamped);
            else
                return 1 - LoopLerp(lerpUnclamped);
        }

        private float LoopLerp(float lerpUnclamped)
        {
            return lerpUnclamped % 1;
        }

        private float FixedTimeLerp(float lerpUnclamped)
        {
            return Mathf.Clamp(lerpUnclamped, 0, 1);
        }
    }
}