using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem
{
    public class TweenerComponent : TweenerComponentData
    {
        public bool isPlaying => tween != null ? tween.playState == PlayState.Play : false;

        private ITween tween;


        public ITween AddSyncedEffect(IEffect effect)
        {
            Stop();

            tween = new SyncTween(this, defaultTweenStyle, defaultDelay, defaultLength);
            tween.AddEffect(effect);

            return tween;
        }


        /// <summary>
        /// Stop currently playing tween
        /// </summary>
        public void Stop(bool invokeDoAfter = true, bool invokePlayAfter = true) => tween?.Stop(invokeDoAfter, invokePlayAfter);


        /// <summary>
        /// Repeat previously played tween, does nothing tween not set
        /// </summary>
        public ITween Repeat()
        {
            Stop(false, false);
            Debug.LogWarningFormat("{0} is starting to play!", tween);
            tween?.Play();

            return tween;
        }
    }
}