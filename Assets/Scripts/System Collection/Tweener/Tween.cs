using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TweenSystem
{
    public abstract class Tween : BaseTweenData, ITween, ITweenData
    {
        protected List<IEffect> effects = new List<IEffect>();

        protected float delay = 0f;
        protected YieldInstruction waitForDelay => new WaitForSeconds(delay);

        public override TweenStyle currentStyle => _tweenStyle;
        public override float currentLength => _length;

        public PlayState playState { get; protected set; }

        protected Func<bool> validateFunc;
        protected Action doAfter;
        protected ITween playAfter;

        protected TweenerComponentData tweener;
        public GameObject gameObject => tweener?.gameObject;


        public Tween(TweenerComponentData tweener, TweenStyle tweenStyle, float delay, float length)
        {
            this.tweener = tweener;

            this.playState = PlayState.Stop;

            this._tweenStyle = tweenStyle;
            this.delay = delay;
            this._length = length;
        }


        public ITween AddEffect(IEffect effect)
        {
            if (!effects.Contains(effect))
                effects.Add(effect);

            return this;
        }

        public ITween AddEffect(IEffect effect, float length)
        {
            if (!effects.Contains(effect))
            {
                effects.Add(effect);
                effect.SetLength(length);
            }

            return this;
        }

        public ITween AddEffect(IEffect effect, TweenStyle tweenStyle)
        {
            if (!effects.Contains(effect))
            {
                effects.Add(effect);
                effect.SetTweenStyle(tweenStyle);
            }

            return this;
        }

        public ITween AddEffect(IEffect effect, float length, TweenStyle tweenStyle)
        {
            if (!effects.Contains(effect))
            {
                effects.Add(effect);
                effect.SetLength(length);
                effect.SetTweenStyle(tweenStyle);
            }

            return this;
        }


        public ITween RemoveEffect(IEffect effect)
        {
            if (effects.Contains(effect))
                effects.Remove(effect);

            return this;
        }

        public void ClearEffects()
        {
            effects.Clear();
        }


        public ITween SetTweenStyle(TweenStyle style)
        {
            this._tweenStyle = style;

            return this;
        }

        public ITween SetDelay(float delay)
        {
            this.delay = delay;

            return this;
        }

        public ITween SetLength(float length)
        {
            this._length = length;

            return this;
        }


        public ITween WaitUntil(Func<bool> validateFunction)
        {
            this.validateFunc = validateFunction;

            return this;
        }


        public ITween DoAfter(Action action)
        {
            this.doAfter = action;

            return this;
        }

        public ITween PlayAfter(ITween tween, float delay = 0)
        {
            this.playAfter = tween;
            playAfter?.SetDelay(delay);

            return this;
        }


        public void Play()
        {
            if (playState == PlayState.Play)
                return;

            // tweener.SetBuilder(this);

            SetEffectData();

            playState = PlayState.Play;
            tweener.StartCoroutine(PlayEffects());
        }

        //TODO: Pause function
        public void Pause() { }

        public void Stop(bool invokeDoAfter = true, bool invokePlayAfter = true)
        {
            if (playState == PlayState.Stop)
                return;

            playState = PlayState.Stop;

            tweener.StopAllCoroutines();

            if (invokeDoAfter)
                doAfter?.Invoke();

            if (invokePlayAfter)
                playAfter?.Play();
        }



        protected bool CheckForEffectEnd(float time, TweenStyle style) => IsFixedEnd(time) && style == TweenStyle.FixedTime;

        protected void SetEffectData()
        {
            foreach (IEffect effect in effects)
                effect.SetTweenData(this);
        }


        protected abstract IEnumerator PlayEffects();
    }
}