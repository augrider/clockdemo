using MessageSystem.Time;

namespace TimeSystem
{
    public abstract class Clock : MessageBehavior, IClock
    {
        protected TimeFormatted time;


        [AutoSubscribeFunction]
        protected void OnTimeUpdated(TimeUpdated message)
        {
            SetCurrentTime(message.currentTime);
        }


        public virtual TimeFormatted GetTime() => time;
        public abstract void SetCurrentTime(TimeFormatted time);
    }
}