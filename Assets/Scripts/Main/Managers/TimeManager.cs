using System.Runtime.CompilerServices;
using System.Collections;
using UnityEngine;
using TimeSystem.TimeProviders;

namespace ManagerSystem
{
    public class TimeManager : Manager<TimeManager>
    {
        public TimeFormatted currentTime { get; private set; } = new TimeFormatted(0, 0, 2);

        private ITimeProvider microsoft;
        private ITimeProvider nist;


        public override void OnLoad()
        {
            this.microsoft = new MicrosoftTime();
            this.nist = new NistTime();

            StartSyncProcedure();

            StartCoroutine(TimeCycle());
        }



        private void SetCurrentTime(TimeFormatted time)
        {
            this.currentTime = time;
            AppManagers.Events.TimeUpdatedNotify(time);
        }


        private IEnumerator TimeCycle()
        {
            while (true)
            {
                yield return new WaitForSecondsRealtime(1);

                var newTime = SecondLater(currentTime);
                CheckForTimeSync(newTime);

                SetCurrentTime(newTime);
            }
        }


        private bool CheckForTimeSync(TimeFormatted newTime)
        {
            var isSyncRequired = newTime.seconds == 0 && newTime.minutes == 0;
            // var isSyncRequired = newTime.seconds % 5.0f == 0;

            if (isSyncRequired)
                StartSyncProcedure();

            return isSyncRequired;
        }

        private void StartSyncProcedure()
        {
            SyncTime(nist, microsoft);
        }

        private void SyncTime(ITimeProvider provider, ITimeProvider nextProvider = null)
        {
            Debug.LogWarningFormat("Attempting to sync time with {0}", provider);

            var awaiter = provider.GetCurrentTime().GetAwaiter();
            StartCoroutine(WaitForTimeSyncEnd(awaiter, nextProvider));
        }

        private IEnumerator WaitForTimeSyncEnd(TaskAwaiter<TimeFormatted> taskAwaiter, ITimeProvider nextProvider = null)
        {
            while (!taskAwaiter.IsCompleted)
                yield return null;

            var timeReceived = taskAwaiter.GetResult();

            if (timeReceived != default(TimeFormatted))
            {
                SetCurrentTime(timeReceived);

                Debug.LogWarningFormat("Time synced");
            }
            else
                Debug.LogWarningFormat("Unable to sync time");

            if (nextProvider != null)
                SyncTime(nextProvider);
        }


        /// <summary>
        /// Get TimeFormatted 1 second later than provided
        /// </summary>
        public static TimeFormatted SecondLater(TimeFormatted time)
        {
            var seconds = time.seconds + 1;
            var minutes = time.minutes;
            var hours = time.hours;

            if (seconds >= 60)
            {
                seconds -= 60;
                minutes++;
            }

            if (minutes >= 60)
            {
                minutes -= 60;
                hours++;
            }

            if (hours >= 24)
                hours -= 24;

            return new TimeFormatted(hours, minutes, seconds);
        }
    }
}