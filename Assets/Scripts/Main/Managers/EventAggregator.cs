﻿using UnityEngine;
using MessageSystem;
using ManagerSystem;
using MessageSystem.Time;

public class EventAggregator : Manager<EventAggregator>
{
    public override void OnLoad()
    {
        Debug.Log("Message Manager started... ");
    }


    #region Call functions
    public void Call<T>(BaseMessage<T> eventType) where T : BaseMessage<T>
    {
        eventType.Call();
    }


    public void TimeUpdatedNotify(TimeFormatted time)
    {
        Call(new TimeUpdated(time));
    }

    public void AlarmClockUpdatedNotify(TimeFormatted time, MonoBehaviour invoker)
    {
        Call(new AlarmTimeUpdated(time, invoker));
    }

    public void AlarmClockSetNotify(TimeFormatted time)
    {
        Call(new AlarmTimeSet(time));
    }
    #endregion
}