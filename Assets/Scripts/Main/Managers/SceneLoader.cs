using System.Linq;
using System.Net.Mime;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using TweenSystem.General;
using TweenSystem;

namespace ManagerSystem
{
    public class SceneLoader : Singleton<SceneLoader>
    {
        [SerializeField] private GameObject loadingScreen;
        [SerializeField] private TweenerComponent loadingScreenTweener;

        [SerializeField] private ScenePackage clockApp;

        private static Scene loaderScene;

        private ITween showLoadScreen;
        private ITween hideLoadScreen;


        // Start is called before the first frame update
        void Start()
        {
            loaderScene = SceneManager.GetActiveScene();

            showLoadScreen = loadingScreenTweener.AddSyncedEffect(new EffectPopToScale());
            hideLoadScreen = loadingScreenTweener.AddSyncedEffect(new EffectPopToScale(Vector3.one, Vector3.forward, false))
                .SetDelay(0.2f)
                .DoAfter(() => loadingScreen.SetActive(false));

            LoadClockApp();
        }



        private static void LoadClockApp()
        {
            current.StartCoroutine(current.clockApp.Load(UnloadAll, current.HideLoadingScreen));
        }


        private static void UnloadAll()
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);

                if (scene.name != loaderScene.name)
                    SceneManager.UnloadSceneAsync(scene);
            }
        }



        private void ShowLoadingScreen()
        {
            loadingScreen.SetActive(true);
            showLoadScreen.Play();
        }

        private void HideLoadingScreen()
        {
            if (loadingScreen.activeInHierarchy)
            {
                Debug.LogAssertionFormat("Here!");
                hideLoadScreen.Play();
            }
        }
    }
}