using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIElementsSystem;

namespace ManagerSystem
{
    public class UIManager : Manager<UIManager>
    {
        public UIElementsComponent elements { get; private set; }


        public override void OnLoad()
        {
        }

        public void RegisterUIElements(UIElementsComponent elements)
        {
            this.elements = elements;
        }
    }
}