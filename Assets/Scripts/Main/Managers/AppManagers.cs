using System.Collections;
using System.Collections.Generic;
using ManagerSystem;
using UnityEngine;

public class AppManagers : ManagerCollection<AppManagers>
{
    public static EventAggregator Events { get; private set; }
    public static UIManager UI { get; private set; }
    public static TimeManager Time { get; private set; }


    void Start()
    {
        Load();
    }

    protected override IManager[] GetManagersSequence()
    {
        Events = current.GetComponentInChildren<EventAggregator>(true);
        UI = current.GetComponentInChildren<UIManager>(true);
        Time = current.GetComponentInChildren<TimeManager>(true);

        return new IManager[] { Events, UI, Time };
    }
}