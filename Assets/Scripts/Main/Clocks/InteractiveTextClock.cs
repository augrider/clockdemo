using System;
using UnityEngine;
using TMPro;

namespace TimeSystem
{
    public class InteractiveTextClock : InteractiveAlarmClock
    {
        [SerializeField] private TMP_InputField hours;
        [SerializeField] private TMP_InputField minutes;


        public override void SetCurrentTime(TimeFormatted time)
        {
            this.time = time;

            hours.SetTextWithoutNotify(time.hours.ToString("D2"));
            minutes.SetTextWithoutNotify(time.minutes.ToString("D2"));
        }


        protected override bool AllowedForChangeNotify() => TryParse(out int hourValue, out int minuteValue);

        protected override TimeFormatted GetCurrentTime()
        {
            if (!TryParse(out int hourValue, out int minuteValue))
                return time;

            return new TimeFormatted(hourValue, minuteValue);
        }



        private bool TryParse(out int hourValue, out int minuteValue)
        {
            hourValue = 0;
            minuteValue = 0;

            return Int32.TryParse(hours.text, out hourValue) && Int32.TryParse(minutes.text, out minuteValue);
        }
    }
}