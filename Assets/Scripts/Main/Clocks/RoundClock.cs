using TimeSystem.Arrows;
using TMPro;
using UnityEngine;

namespace TimeSystem
{
    public class RoundClock : Clock, IClock
    {
        private IClockArrow hour;
        private IClockArrow minute;
        private IClockArrow second;

        [SerializeField] private ClockArrow hourArrow;
        [SerializeField] private ClockArrow minuteArrow;
        [SerializeField] private ClockArrow secondArrow;

        [SerializeField] private TextMeshProUGUI amPmIndicator;


        void Start()
        {
            Initialize();
        }


        public override void SetCurrentTime(TimeFormatted time)
        {
            (var hours, var minutes) = GetCorrectHourMinute(time);

            hour?.SetCurrentValueFloat(hours);
            minute?.SetCurrentValueFloat(minutes);
            second?.SetCurrentValue(time.seconds);
        }



        private void Initialize()
        {
            this.hour = hourArrow as IClockArrow;
            this.minute = minuteArrow as IClockArrow;
            this.second = secondArrow as IClockArrow;

            AfterInit();
        }

        private (float, float) GetCorrectHourMinute(TimeFormatted time)
        {
            var correctHours = (float)time.hours12 + time.minutes / 60.0f;
            var correctMinutes = (float)time.minutes + time.seconds / 60.0f;

            return (correctHours, correctMinutes);
        }


        /// <summary>
        /// Change AmPm indicator
        /// </summary>
        /// <param name="isPM">If true, show PM. Else, show AM instead</param>
        private void TogglePM(bool isPM)
        {
            amPmIndicator?.SetText(isPM ? "PM" : "AM");
        }


        protected virtual void AfterInit() { }
    }
}