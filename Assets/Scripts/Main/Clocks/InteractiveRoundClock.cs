using TimeSystem.Arrows;
using TMPro;
using UnityEngine;

namespace TimeSystem
{
    public class InteractiveRoundClock : InteractiveAlarmClock, IClock
    {
        private IClockArrow hour;
        private IClockArrow minute;

        [SerializeField] private ClockArrow hourArrow;
        [SerializeField] private ClockArrow minuteArrow;

        [SerializeField] private TextMeshProUGUI amPmIndicator;
        private bool isPM;


        void Start()
        {
            Initialize();
        }


        public override void SetCurrentTime(TimeFormatted time)
        {
            this.time = time;

            hour.SetCurrentValue(time.hours12);
            minute.SetCurrentValue(time.minutes);

            TogglePM(time.isPM);
        }


        public void SwitchAmPm()
        {
            TogglePM(!isPM);

            OnTimeFinishedEditing();
        }



        /// <summary>
        /// Change AmPm indicator
        /// </summary>
        /// <param name="isPM">If true, show PM. Else, show AM instead</param>
        private void TogglePM(bool isPM)
        {
            this.isPM = isPM;
            amPmIndicator?.SetText(isPM ? "PM" : "AM");
        }


        protected override bool AllowedForChangeNotify() => true;

        protected override TimeFormatted GetCurrentTime()
        {
            var currentHour = hour.currentValue;
            currentHour += isPM ? 12 : 0;

            return new TimeFormatted(currentHour, minute.currentValue);
        }


        private void Initialize()
        {
            this.hour = hourArrow as IClockArrow;
            this.minute = minuteArrow as IClockArrow;
        }
    }
}