using TMPro;
using UnityEngine;

namespace TimeSystem
{
    public class TextClock : Clock, IClock
    {
        [SerializeField] private TextMeshProUGUI clock;


        public override void SetCurrentTime(TimeFormatted time)
        {
            this.time = time;
            this.clock?.SetText(TimeToString(time));
        }



        private string TimeToString(TimeFormatted time)
        {
            return time.Format12();
        }
    }
}