using System;


/// <summary>
/// Structure, containing hours, minutes and seconds in int format
/// </summary>
/// <remarks>
/// Note: We could use DateTime struct instead, but that way we can transfer only required data to clocks
/// </remarks>
[Serializable]
public struct TimeFormatted
{
    public int hours;
    public int minutes;
    public int seconds;

    public int hours12 => hours % 12;
    public bool isPM => hours / 12 > 0;


    public TimeFormatted(int hours, int minutes, int seconds)
    {
        this.hours = MathCustom.ClampInterval(hours, 0, 23);
        this.minutes = MathCustom.ClampInterval(minutes, 0, 59);
        this.seconds = MathCustom.ClampInterval(seconds, 0, 59);
    }

    public TimeFormatted(int hours, int minutes)
    {
        this.hours = MathCustom.ClampInterval(hours, 0, 23);
        this.minutes = MathCustom.ClampInterval(minutes, 0, 59);
        this.seconds = 0;
    }

    public TimeFormatted(DateTime dateTime)
    {
        this.hours = dateTime.Hour;
        this.minutes = dateTime.Minute;
        this.seconds = dateTime.Second;
    }


    public static bool operator ==(TimeFormatted timeA, TimeFormatted timeB)
    {
        return timeA.hours == timeB.hours && timeA.minutes == timeB.minutes && timeA.seconds == timeB.seconds;
    }

    public static bool operator !=(TimeFormatted timeA, TimeFormatted timeB)
    {
        return timeA.hours != timeB.hours || timeA.minutes != timeB.minutes || timeA.seconds != timeB.seconds;
    }


    public string Format24()
    {
        return String.Format("{0}:{1,2}:{2,2}", hours.ToString("D2"), minutes.ToString("D2"), seconds.ToString("D2"));
    }

    public string Format12()
    {
        int result = Math.DivRem(hours, 12, out int hour);
        var isPM = result > 0;

        return String.Format("{0}:{1,2}:{2,2} {3}", hour, minutes.ToString("D2"), seconds.ToString("D2"), isPM ? "PM" : "AM");
    }
}