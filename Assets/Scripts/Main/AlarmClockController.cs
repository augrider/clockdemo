using System.Collections;
using MessageSystem.Time;
using TimeSystem.AlarmSaveHandlers;
using UnityEngine;

namespace TimeSystem
{
    /// <summary>
    /// Class which handles work with alarm
    /// </summary>
    public class AlarmClockController : MessageBehavior
    {
        [SerializeField] private InteractiveAlarmClock stringClock;
        [SerializeField] private InteractiveAlarmClock roundClock;

        [SerializeField] private GameObject clocksContent;
        [SerializeField] private GameObject alarmDisabledContent;

        private IClock interactiveRoundClock;
        private IClock interactiveStringClock;

        private bool alarmEnabled;
        private TimeFormatted alarmTime;

        private bool isEdited = false;

        private IAlarmSaveHandler alarmSaveHandler;


        /// <summary>
        /// Initial preload of saved alarm time and all clocks
        /// </summary>
        IEnumerator Start()
        {
            PreloadClocks();

            yield return null;

            this.alarmSaveHandler = new AlarmToJSON();
            Load();
        }

        /// <summary>
        /// Enable/disable alarm externally
        /// </summary>
        public void ToggleAlarm()
        {
            ToggleAlarmEnabled(!alarmEnabled);
            Save();
        }


        [AutoSubscribeFunction]
        private void OnAlarmClockChanged(AlarmTimeUpdated message)
        {
            this.isEdited = true;
        }

        [AutoSubscribeFunction]
        private void OnAlarmClockSet(AlarmTimeSet message)
        {
            this.isEdited = false;
            this.alarmTime = message.time;

            Save();
            SetAlarmTime(alarmTime);
            Debug.LogWarningFormat("Saved!");
        }


        [AutoSubscribeFunction]
        private void OnTimeChanged(TimeUpdated message)
        {
            if (!CanCheckForRingAlarm(message.currentTime))
                return;

            if (message.currentTime == this.alarmTime)
                AppManagers.Events.Call(new AlarmTriggered(alarmTime));
        }



        private bool CanCheckForRingAlarm(TimeFormatted currentTime)
        {
            return !this.isEdited && this.alarmEnabled && currentTime.seconds == 0;
        }


        private void SetAlarmTime(TimeFormatted time)
        {
            interactiveRoundClock.SetCurrentTime(time);
            interactiveStringClock.SetCurrentTime(time);
        }

        private void ToggleAlarmEnabled(bool value)
        {
            this.alarmEnabled = value;

            clocksContent?.SetActive(alarmEnabled);
            alarmDisabledContent?.SetActive(!alarmEnabled);
        }


        private void Load()
        {
            (alarmEnabled, alarmTime) = alarmSaveHandler.Load();

            SetAlarmTime(alarmTime);
            ToggleAlarmEnabled(alarmEnabled);
        }

        private void Save() => alarmSaveHandler.Save(alarmEnabled, alarmTime);


        private void PreloadClocks()
        {
            this.interactiveRoundClock = roundClock as IClock;
            this.interactiveStringClock = stringClock as IClock;
        }
    }
}