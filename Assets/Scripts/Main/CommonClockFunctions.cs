using System;
using UnityEngine;

namespace TimeSystem
{
    public static class CommonClockFunctions
    {
        public static float GetAngleFromClockValue(float value, int maxValue)
        {
            var clamped = MathCustom.ClampInterval(value, 0.0f, (float)maxValue);
            return -360 * (clamped / maxValue);
        }

        public static float GetClockValueFromQuaternion(Quaternion arrowQuaternion, int maxValue)
        {
            var angle = 360 - arrowQuaternion.eulerAngles.z;
            return Math.Abs(angle) * maxValue / 360;
        }


        public static Quaternion GetArrowOrientation(float angle)
        {
            return Quaternion.Euler(0, 0, angle);
        }
    }
}