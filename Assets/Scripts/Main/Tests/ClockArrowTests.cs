using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using TimeSystem;
using UnityEngine;
using UnityEngine.TestTools;

public class ClockArrowTests
{
    // A Test behaves as an ordinary method
    [Test]
    public void AngleFromValue_ShouldMinus90_When15OutOf60()
    {
        var angle = CommonClockFunctions.GetAngleFromClockValue(15, 60);

        Assert.AreEqual(-90, angle);
    }

    [Test]
    public void AngleFromValue_ShouldMinus330_When11OutOf12()
    {
        var angle = CommonClockFunctions.GetAngleFromClockValue(11, 12);

        Assert.AreEqual(-330, angle);
    }


    [Test]
    public void ValueFromAngle_Should15_WhenAngleMinus90_MaxValue60()
    {
        var arrowQuaternion = CommonClockFunctions.GetArrowOrientation(-90);
        var value = CommonClockFunctions.GetClockValueFromQuaternion(arrowQuaternion, 60);

        Assert.AreEqual(15, value);
    }

    [Test]
    public void ValueFromAngle_Should59_WhenAngleMinus354_MaxValue60()
    {
        var arrowQuaternion = CommonClockFunctions.GetArrowOrientation(-354);
        var value = CommonClockFunctions.GetClockValueFromQuaternion(arrowQuaternion, 60);

        Assert.AreEqual(59, value);
    }
}
