using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TimeSystem.AlarmSaveHandlers
{
    public class AlarmToJSON : IAlarmSaveHandler
    {
        private string fileName => "AlarmData.json";
        private string filePath => System.IO.Path.Combine(Application.persistentDataPath, fileName);


        public (bool, TimeFormatted) Load()
        {
            var dataString = System.IO.File.ReadAllText(filePath);

            if (string.IsNullOrEmpty(dataString))
                return (false, new TimeFormatted(0, 0));

            var alarmData = JsonUtility.FromJson<AlarmData>(dataString);
            return (alarmData.enabled, alarmData.time);
        }

        public void Save(bool enabled, TimeFormatted time)
        {
            var dataString = JsonUtility.ToJson(new AlarmData(enabled, time));
            System.IO.File.WriteAllText(filePath, dataString);
        }


        [Serializable]
        private struct AlarmData
        {
            public bool enabled;
            public TimeFormatted time;


            public AlarmData(bool enabled, TimeFormatted time)
            {
                this.enabled = enabled;
                this.time = time;
            }
        }
    }
}