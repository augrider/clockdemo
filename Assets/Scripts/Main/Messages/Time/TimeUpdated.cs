using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessageSystem.Time
{
    public class TimeUpdated : BaseMessage<TimeUpdated>
    {
        public TimeFormatted currentTime;


        public TimeUpdated(TimeFormatted currentTime)
        {
            this.currentTime = currentTime;
        }

        public TimeUpdated() { }
    }
}