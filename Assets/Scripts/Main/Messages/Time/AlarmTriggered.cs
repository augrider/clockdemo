using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessageSystem.Time
{
    public class AlarmTriggered : BaseMessage<AlarmTriggered>
    {
        public TimeFormatted currentTime;


        public AlarmTriggered(TimeFormatted currentTime)
        {
            this.currentTime = currentTime;
        }

        public AlarmTriggered() { }
    }
}