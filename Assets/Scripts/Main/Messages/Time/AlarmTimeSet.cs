using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessageSystem.Time
{
    public class AlarmTimeSet : BaseMessage<AlarmTimeSet>
    {
        public TimeFormatted time;


        public AlarmTimeSet(TimeFormatted time)
        {
            this.time = time;
        }

        public AlarmTimeSet() { }
    }
}