using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MessageSystem.Time
{
    public class AlarmTimeUpdated : BaseMessage<AlarmTimeUpdated>
    {
        public TimeFormatted time;
        public MonoBehaviour invoker;


        public AlarmTimeUpdated(TimeFormatted time, MonoBehaviour invoker)
        {
            this.time = time;
            this.invoker = invoker;
        }

        public AlarmTimeUpdated() { }
    }
}