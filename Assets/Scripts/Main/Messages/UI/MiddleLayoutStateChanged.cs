﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UIElementsSystem;

namespace MessageSystem.UI
{
    public class MiddleLayoutStateChanged : BaseMessage<MiddleLayoutStateChanged>
    {
        public MiddlePanelType currentPanelType;
        public Vector2 currentPosition;
        public bool isMoving;


        public MiddleLayoutStateChanged(MiddlePanelType currentPanelType, Vector2 currentPosition, bool isMoving = false)
        {
            this.currentPanelType = currentPanelType;
            this.currentPosition = currentPosition;
            this.isMoving = isMoving;
        }

        public MiddleLayoutStateChanged() { }
    }
}