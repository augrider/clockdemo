using MessageSystem.Time;

namespace TimeSystem
{
    public abstract class InteractiveAlarmClock : MessageBehavior, IClock
    {
        protected TimeFormatted time;


        [AutoSubscribeFunction]
        protected void OnAlarmClockUpdated(AlarmTimeUpdated message)
        {
            if (this == message.invoker)
                return;

            this.time = message.time;
            SetCurrentTime(time);
        }


        /// <summary>
        /// Logic for when editing in progress. Invoked when time changed externally
        /// </summary>
        /// <remarks>
        /// There you want to update everyone else
        /// </remarks>
        public void OnTimeChanged()
        {
            if (AllowedForChangeNotify())
                AppManagers.Events.AlarmClockUpdatedNotify(GetCurrentTime(), this);
        }


        /// <summary>
        /// Logic for when editing was finished. Invoked when external change process was finished
        /// </summary>
        /// <remarks>
        /// There you want to update everyone
        /// </remarks>
        public void OnTimeFinishedEditing()
        {
            AppManagers.Events.AlarmClockSetNotify(GetCurrentTime());
        }


        public virtual TimeFormatted GetTime() => GetCurrentTime();
        public abstract void SetCurrentTime(TimeFormatted time);

        protected abstract bool AllowedForChangeNotify();
        protected abstract TimeFormatted GetCurrentTime();
    }
}