using UnityEngine;

namespace TimeSystem.Arrows
{
    public class InteractiveClockArrow : ClockArrow
    {
        [SerializeField] private InteractiveClockArrowHandle handle;


        void Start()
        {
            handle.Initialize(this);
        }
    }
}