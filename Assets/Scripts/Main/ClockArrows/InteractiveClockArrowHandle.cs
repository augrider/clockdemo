using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

namespace TimeSystem.Arrows
{
    public class InteractiveClockArrowHandle : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private IClockArrow interactiveClockArrow;

        private Vector2 center => transform.parent.position;

        [SerializeField] private UnityEvent onValueChanged;
        [SerializeField] private UnityEvent onSetTimeFinished;


        public void Initialize(IClockArrow interactiveClockArrow)
        {
            this.interactiveClockArrow = interactiveClockArrow;
        }


        public void OnBeginDrag(PointerEventData eventData)
        {
            onValueChanged?.Invoke();
        }

        public void OnDrag(PointerEventData eventData)
        {
            var targetPosition = Mouse.current.position.ReadValue() - center;
            SetCorrectPositionAndRotation(targetPosition);
            onValueChanged?.Invoke();
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            onSetTimeFinished?.Invoke();
        }



        private void SetCorrectPositionAndRotation(Vector2 targetPosition)
        {
            var correctAngle = GetCorrectAngle(targetPosition.normalized);
            var correctQuaternion = CommonClockFunctions.GetArrowOrientation(correctAngle);

            interactiveClockArrow.SetQuaternionRotation(correctQuaternion);
        }


        private float GetCorrectAngle(Vector2 correctPosition)
        {
            var angle = Vector2.Angle(Vector2.up, correctPosition);

            if (correctPosition.x >= 0)
                return -angle;

            return -360 + angle;
        }
    }
}