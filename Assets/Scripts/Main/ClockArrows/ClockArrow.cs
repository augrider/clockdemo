using UnityEngine;

namespace TimeSystem.Arrows
{
    /// <summary>
    /// Expandable behavior for all clock arrows
    /// </summary>
    public class ClockArrow : MonoBehaviour, IClockArrow
    {
        public int maxValue => _maxValue;
        [SerializeField] private int _maxValue;

        public int currentValue => MathCustom.ClampInterval(Mathf.FloorToInt(floatValue), 0, maxValue);
        protected float floatValue = 0;


        public virtual void SetCurrentValue(int value)
        {
            floatValue = value;
            SetCorrectRotation();
        }

        public virtual void SetCurrentValueFloat(float value)
        {
            floatValue = MathCustom.ClampInterval(value, 0, maxValue);
            SetCorrectRotation();
        }

        public virtual void SetQuaternionRotation(Quaternion rotation)
        {
            floatValue = CommonClockFunctions.GetClockValueFromQuaternion(rotation, maxValue);
            SetCorrectRotation();
        }



        private void SetCorrectRotation()
        {
            var angle = CommonClockFunctions.GetAngleFromClockValue(MathCustom.ClampInterval(floatValue, 0, maxValue), maxValue);

            transform.rotation = CommonClockFunctions.GetArrowOrientation(angle);
        }
    }
}