
using System;
using System.Globalization;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

namespace TimeSystem.TimeProviders
{
    public class MicrosoftTime : ITimeProvider
    {
        public async Task<TimeFormatted> GetCurrentTime()
        {
            try
            {
                var myHttpWebRequest = (HttpWebRequest)WebRequest.Create("http://www.microsoft.com");

                Debug.LogWarningFormat("Fetching from Microsoft!");
                var response = await myHttpWebRequest.GetResponseAsync();

                string todaysDates = response.Headers["date"];
                var dateTime = DateTime.ParseExact(todaysDates,
                                           "ddd, dd MMM yyyy HH:mm:ss 'GMT'",
                                           CultureInfo.InvariantCulture.DateTimeFormat,
                                           DateTimeStyles.AssumeUniversal);

                return new TimeFormatted(dateTime);
            }
            catch
            {
                return default(TimeFormatted);
            }
        }
    }
}