using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;
using UnityEngine;

namespace TimeSystem.TimeProviders
{
    public class NistTime : ITimeProvider
    {
        public async Task<TimeFormatted> GetCurrentTime()
        {
            try
            {
                var client = new TcpClient();

                Debug.LogWarningFormat("Fetching from time.nist.gov!");

                await client.ConnectAsync("time.nist.gov", 13);

                using var streamReader = new StreamReader(client.GetStream());
                var response = await streamReader.ReadToEndAsync();

                var utcDateTimeString = response.Substring(7, 17);
                var dateTime = DateTime.ParseExact(utcDateTimeString, "yy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);

                return new TimeFormatted(dateTime);
            }
            catch
            {
                return default(TimeFormatted);
            }
        }
    }
}