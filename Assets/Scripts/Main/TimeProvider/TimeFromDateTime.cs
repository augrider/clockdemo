using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace TimeSystem.TimeProviders
{
    public class TimeFromDateTime : ITimeProvider
    {
        public async Task<TimeFormatted> GetCurrentTime()
        {
            var time = new TimeFormatted(DateTime.Now);
            return time;
        }
    }
}