using System.Collections;
using ManagerSystem;
using UnityEngine;
using UnityEngine.UI;

namespace UIElementsSystem
{
    /// <summary>
    /// Class for storing references on all important UI components. Automatically registers in UI manager
    /// </summary>
    public class UIElementsComponent : MonoBehaviour
    {
        public IMiddlePanelsHandler middlePanels => _middlePanels;
        [SerializeField] private MiddleLayoutBehaviour _middlePanels;


        IEnumerator Start()
        {
            yield return UIManager.WaitForLoad();

            AppManagers.UI.RegisterUIElements(this);
        }
    }
}