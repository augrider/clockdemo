using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MessageSystem.Time;

namespace UIElementsSystem
{
    public class AlarmScreen : MessageBehavior
    {
        [SerializeField] private TextMeshProUGUI timeString;
        [SerializeField] private GameObject screen;

        [SerializeField] private float showLength = 10;


        /// <summary>
        /// Externally stop running alarm
        /// </summary>
        public void StopAlarm()
        {
            StopAllCoroutines();
            screen.SetActive(false);
        }


        [AutoSubscribeFunction]
        private void OnAlarmTriggered(AlarmTriggered message)
        {
            StartCoroutine(ShowAlarm(message.currentTime));
        }



        private IEnumerator ShowAlarm(TimeFormatted time)
        {
            timeString.SetText(time.Format24());
            screen.SetActive(true);

            yield return new WaitForSecondsRealtime(showLength);

            screen.SetActive(false);
        }
    }
}