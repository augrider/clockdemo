using System.Collections;
using System.Collections.Generic;
using ManagerSystem;
using MessageSystem.UI;
using UnityEngine;
using UnityEngine.UI;


namespace UIElementsSystem
{
    public class FooterController : MessageBehavior
    {
        private IMiddlePanelsHandler panelsHandler => AppManagers.UI.elements.middlePanels;

        [SerializeField] private Toggle mainClock;
        [SerializeField] private Toggle alarmClock;


        public void Start()
        {
            this.enabled = true;
        }

        [AutoSubscribeFunction]
        void OnChangeOrder(MiddleLayoutStateChanged message)
        {
            ChangeActiveButton(message.currentPanelType);
        }


        //TODO: Fix footer buttons
        public void ChangePanel(int typeIndex)
        {
            var targetType = (MiddlePanelType)typeIndex;

            if (GetToggle(targetType).isOn)
                panelsHandler.SetPanel(targetType);
        }



        private void ChangeActiveButton(MiddlePanelType panelType)
        {
            GetToggle(panelType).Select();
        }


        private Toggle GetToggle(MiddlePanelType type)
        {
            switch (type)
            {
                default:
                case (MiddlePanelType.MainClock):
                    return mainClock;

                case (MiddlePanelType.AlarmClock):
                    return alarmClock;
            }
        }
    }
}