namespace UIElementsSystem
{
    /// <summary>
    /// List of all middle panels in order
    /// </summary>
    public enum MiddlePanelType { MainClock, AlarmClock }
}