﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UIElementsSystem
{
    public class MiddlePanelBehaviour : PanelBehaviour
    {
        public int defaultOrder { get; private set; }
        public MiddlePanelType panelType;

        protected Vector2 defaultPosition => new Vector2(defaultOrder, 0);


        public void SetOrder(int newOrder)
        {
            defaultOrder = newOrder;
            panel.SetRelativePosition(defaultPosition);
        }


        // [AutoSubscribeFunction]
        // protected void OnStateChanged(MiddleLayoutStateChanged message)
        // {
        //     var orderDeviation = -message.currentPosition.x - defaultOrder;

        //     OnDeviationChanged(orderDeviation);
        //     SwitchPanelState(orderDeviation, message.isMoving);
        // }


        protected virtual void OnDeviationChanged(float orderDeviation) { }



        private void SwitchPanelState(float orderDeviation, bool isMoving)
        {
            if (isMoving)
            {
                SetMoving();
                return;
            }

            if (orderDeviation == 0)
            {
                SetOn();
                return;
            }

            SetOff();
        }
    }
}