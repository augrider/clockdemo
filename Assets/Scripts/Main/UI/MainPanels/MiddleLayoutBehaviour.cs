using System.Collections;
using UnityEngine;
using System.Linq;
using ManagerSystem;
using MessageSystem.UI;

namespace UIElementsSystem
{
    public class MiddleLayoutBehaviour : PanelBehaviour, IMiddlePanelsHandler
    {
        [SerializeField] private MiddlePanelBehaviour[] panelSequence;
        [SerializeField] private MiddlePanelType defaultPanelType;

        public MiddlePanelType currentPanelType { get; private set; }

        public bool allowPanelSwap => !panel.isMoving;

        private Rect currentResolution;


        public void Start()
        {
            currentResolution = Screen.safeArea;

            PlaceAllPanels();
            SetPanel(defaultPanelType);
        }

        public void SetPanel(MiddlePanelType targetType)
        {
            if (allowPanelSwap && this.currentPanelType != targetType)
                MoveToOrder(targetType);
        }


        void FixedUpdate()
        {
            if (currentResolution == Screen.safeArea)
                return;

            this.currentResolution = Screen.safeArea;
            PlaceAllPanels();

            var targetPosition = -GetRelativePosition(GetPanel(currentPanelType).defaultOrder);
            panel.SetRelativePosition(targetPosition);
        }


        private void PlaceAllPanels()
        {
            foreach (MiddlePanelBehaviour data in panelSequence)
                data.SetOrder((int)data.panelType);
        }


        private void MoveToOrder(MiddlePanelType target)
        {
            currentPanelType = target;

            var targetPanel = GetPanel(target);

            if (targetPanel)
            {
                panel.MoveToRelative(-GetRelativePosition(targetPanel.defaultOrder));
                StartCoroutine(Notify());
            }
        }


        private MiddlePanelBehaviour GetPanel(MiddlePanelType type)
        {
            return panelSequence.FirstOrDefault(t => t.panelType == type);
        }


        private IEnumerator Notify()
        {
            yield return new WaitForEndOfFrame();

            while (isMoving)
            {
                AppManagers.Events.Call(new MiddleLayoutStateChanged(currentPanelType, panel.relativePosition, true));
                yield return new WaitForEndOfFrame();
            }

            AppManagers.Events.Call(new MiddleLayoutStateChanged(currentPanelType, panel.relativePosition, false));
        }


        private Vector2 GetRelativePosition(float relativeHorizontal) => new Vector2(relativeHorizontal, 0);
    }
}