using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for all clock arrows
/// </summary>
public interface IClockArrow
{
    /// <summary>
    /// Maximum for this arrow
    /// </summary>
    int maxValue { get; }
    /// <summary>
    /// Current value, shown on this clock, rounded down
    /// </summary>
    int currentValue { get; }

    void SetCurrentValue(int value);
    void SetCurrentValueFloat(float value);
    void SetQuaternionRotation(Quaternion rotation);
}