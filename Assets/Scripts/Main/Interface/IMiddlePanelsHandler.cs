using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UIElementsSystem
{
    public interface IMiddlePanelsHandler
    {
        MiddlePanelType currentPanelType { get; }


        void SetPanel(MiddlePanelType targetType);
    }
}