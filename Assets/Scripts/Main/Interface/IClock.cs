using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Interface for all clocks
/// </summary>
public interface IClock
{
    /// <summary>
    /// Set clock to show provided time
    /// </summary>
    void SetCurrentTime(TimeFormatted time);
    /// <summary>
    /// Get time, currently shown on clock
    /// </summary>
    /// <returns>Time on clock</returns>
    TimeFormatted GetTime();
}