using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

/// <summary>
/// Interface for classes that can provide current time
/// </summary>
public interface ITimeProvider
{
    /// <summary>
    /// Get current time in TimeFormatted type
    /// </summary>
    /// <remarks>
    /// Uses async operation
    /// </remarks>
    /// <returns>Task, which returns TimeFormatted</returns>
    Task<TimeFormatted> GetCurrentTime();
}