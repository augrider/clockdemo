using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAlarmSaveHandler
{
    void Save(bool enabled, TimeFormatted time);
    (bool, TimeFormatted) Load();
}