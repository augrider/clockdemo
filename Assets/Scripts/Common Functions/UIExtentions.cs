﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public static class UIExtentions
{
    public static float DeadZoneRel = 0.05f;
    public static float DeadZoneRelSqr => DeadZoneRel * DeadZoneRel;
}