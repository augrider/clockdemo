using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class ComponentExtentions
{
    /// <summary>
    /// Set camera view port relative to screen
    /// </summary>
    /// <param name="left">Relative position of left border of view port</param>
    /// <param name="right">Relative position of right border of view port</param>
    public static void SetCameraView(this Camera camera, float left, float right)
    {
        left = Mathf.Clamp(left, 0, 1);
        right = Mathf.Clamp(right, 0, 1);

        camera.rect = new Rect(left, 0, right, 1);
    }
}