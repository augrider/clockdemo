using System;
using System.Linq;
using System.Collections.Generic;


/// <summary>
/// Various functions for various types of arrays and dictionaries
/// </summary>
public static class ArrayExtentions
{
    public static void RemoveAfter<T, Z>(this IDictionary<T, Z> dictionary, T key)
    {
        bool after = false;
        var keys = dictionary.Keys.ToArray();
        int i = 0;

        while (i < dictionary.Count)
        {
            var currentKey = keys[i];

            if (after)
            {
                dictionary.Remove(currentKey);
                continue;
            }

            if (currentKey.Equals(key))
                after = true;

            i++;
        }
    }

    public static void RemoveAfter<T>(this IList<T> list, T item)
    {
        bool after = false;
        int i = 0;

        while (i < list.Count)
        {
            var currentItem = list[i];

            if (after)
            {
                list.Remove(currentItem);
                continue;
            }

            if (currentItem.Equals(item))
                after = true;

            i++;
        }
    }


    public static void RemoveBefore<T>(this IList<T> list, T item)
    {
        int i = 0;

        while (i < list.Count)
        {
            var currentItem = list[i];
            i++;

            if (currentItem.Equals(item))
                break;

            list.Remove(currentItem);
        }
    }


    public static void RemoveAfter<T, Z>(this IDictionary<T, Z> dictionary, T key, Action<T, Z> action)
    {
        bool after = false;
        var keys = dictionary.Keys.ToArray();
        int i = 0;

        while (i < dictionary.Count)
        {
            var currentKey = keys[i];
            i++;

            if (currentKey.Equals(key))
            {
                after = true;
                continue;
            }

            if (after)
            {
                action(currentKey, dictionary[currentKey]);
                dictionary.Remove(currentKey);
            }
        }
    }

    public static void RemoveAfter<T>(this IList<T> list, T item, Action<T> action)
    {
        bool after = false;
        int i = 0;

        while (i < list.Count)
        {
            var currentItem = list[i];
            i++;

            if (currentItem.Equals(item))
            {
                after = true;
                continue;
            }

            if (after)
            {
                action(currentItem);
                list.Remove(currentItem);
            }
        }
    }


    public static void RemoveBefore<T>(this IList<T> list, T item, Action<T> action)
    {
        int i = 0;

        while (i < list.Count)
        {
            var currentItem = list[i];
            i++;

            if (currentItem.Equals(item))
                break;

            action(currentItem);
            list.Remove(currentItem);
        }
    }


    public static string AllContent<T>(this T[] array)
    {
        string s = string.Format("{0} items: ", array.Length);
        s += string.Join(", ", array);
        return s;
    }

    public static string AllContent<T>(this IList<T> array)
    {
        string s = string.Format("{0} items: ", array.Count);
        s += string.Join(", ", array);
        return s;
    }

    public static string AllContent<T>(this IEnumerable<T> array)
    {
        string s = string.Format("{0} items: ", array.Count());
        s += string.Join(", ", array);
        return s;
    }
}